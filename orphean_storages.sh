#!/bin/bash


readonly PROCESSAPI='https://front.tm.bsf-intranet.org/goto/temps-modernes/api/process'                                                                    
readonly CURL='curl --silent --insecure'


storages_list=""
orpheans=""

cd /mnt/demostorage || exit 1

for i in ???????????????????????????????? ; do storages_list="${storages_list} $(basename "$i" )" ; done


echo "[+] Found $( echo "$storages_list" | wc -w ) storages. Looking for orphean ones..."

(
	echo "process_id project_name exp_date owner size"

	for id in $storages_list ; do

		ct_count=$( docker ps --filter "name=$id" --format '{{.Names}}' | wc -l )
		[ "$ct_count" -ne 0 ] && continue

		# orphean storage found, store it in a variable for later use
		# FIXME: this won't make it out of the subshell
		orpheans="$orpheans $id"

		# display misc infos about the orphean storage's process_id
		infos=$( $CURL "${PROCESSAPI}/?id=${id}" )
		owner=$( echo "$infos" | jq -r '.user.username' )
		project_name=$( echo "$infos" | jq -r '.project.name' )
		expiration_date=$( echo "$infos" | jq -r '.media_center_configuration.expire' | cut -d ' ' -f 1 )
		size=$( du -s -BM "$id" | cut -f 1 )
		echo "$id $project_name $expiration_date $owner $size"
	done \
		| sort --version-sort -k 3 \
) \
	| column -t

exit # we exit early because `du` puts a lot of load on the server

# and also because of the subshell var bug
echo "orpheans: $orpheans"

# let's put more load on the server \o/
echo "[+] wasted/recoverable space for ${orpheans}:"
du -s -BM -c "$orpheans"
