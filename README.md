# madrid tools

Some tooling around Madrid VM.

## install

Until it is included in madrid's AMI/cloud-init:

```shell
git clone https://gitlab.com/bibliosansfrontieres/tm/madrid-tools/ /usr/local/src/madrid-tools
ln -s /usr/local/src/madrid-tools/backup_vm.sh /usr/local/sbin/
ln -s /usr/local/src/madrid-tools/restore_vm.sh /usr/local/sbin/
ln -s /usr/local/src/madrid-tools/orphean_storages.sh /usr/local/sbin/
```

## backup_vm.sh

This scripts backups a VM configuration. It automatically grabs the
Omeka `project.id`, the `owner` and `project_name` from the `process_id`:

```text
# backup_vm.sh 25152327be5441e790ddf6825744d697
[+] DB saved to /mnt/demostorage/backup-app.db/25152327be5441e790ddf6825744d697.69348.idc-bsf-project-michel.polnareff.20210422144041.db
```

## restore_vm.sh

This script will restore a VM configuration backup file to an existing VM.

Basically:

* copy the backup DB file over the VM one
* rename the containers names in the DB
* check for content that need to be reinstalled - ie, in the DB but not on the filesystem

```text
Usage: restore_vm.sh <backup DB file path> <target process_id>

    Example: restore_vm.sh /mnt/demostorage/backup-app.db/idb-bdi-education-6085e174063d4d388536448298ee1ba3-20210323-app.db 734878d9f72f4f8e92405babac4da457
```

### pitfalls

The target VM needs to have the same applications installed than the
source VM.

Example:

Before restore, the target VM has these containers:

```text
kiwix.app.kiwix.25152327be5441e790ddf6825744d697
mediacenter.app.mediacenter.25152327be5441e790dd
```

The backuped VM had these containers:

```text
mediacenter.app.mediacenter.22e26b7fd0704d0e9b2a8ea71990d45b
kolibri.app.kolibri.22e26b7fd0704d0e9b2a8ea71990d45b
kiwix.app.kiwix.22e26b7fd0704d0e9b2a8ea71990d45b
surfer.app.surfer.22e26b7fd0704d0e9b2a8ea71990d45b
```

In the Temps Modernes context, the `25152327...` VM has no control over
the Docker Daemon on the host, and can not create the `surfer` and
`kolibri` containers.

This leads to a timing related pitfall: if the target VM has just been created
but didn't install it's app yet, then the container renaming will fail (since
the containesr don't exist yet in the target VM). As a result the target VM is
broken.

## orphean_storages.sh

This script checks for the existing demostorages and will list the orpheaned ones.

A storage is considered orpheaned when:

* the folder exists
* the process_id has no container running

This is a quick'n'dirty workaround for
[`bibliosansfrontieres/tm/factory_manager#45`](https://gitlab.com/bibliosansfrontieres/tm/factory_manager/-/issues/45)

### Example session

First, list the orphean storages:

```text
root@madrid-prod:/mnt/demostorage# time orphean_storages.sh
[+] Found 32 storages. Looking for orphean ones...
process_id                        project_name        exp_date    owner           size
828a28724b4742aa91234330c118f5d0  idc-cog-univ-educ   2022-05-07  manon.tanguy    3M
614a22a88ee0447aafb78320e595cc33  idc-cog-univ-educ   2022-05-07  manon.tanguy    53185M
8296b3ef2a5447ad8faec3e9258049c6  idc-cog-univ-educ   2022-05-07  manon.tanguy    53185M
2d49af1e09ea4976af43fe59769eadf3  idc-fra-demo-congo  2022-07-17  manon.tanguy    255287M
74d8001ad1dd4a1d8d3d165ad2fe3b6e  idc-bdi-education4  2023-01-23  Jeremy.bouin    330127M
93f72ade36ff46229ce7ae7b70f57df2  idc-bdi-pmedemo     2023-03-13  thomas.faucher  290627M
52dbdef6c4db4fccbd0368b61245c36e  idb-fra-parentalpp  2023-05-19  hadjer.dakhli   72451M

real    0m2.579s
user    0m1.223s
sys     0m1.100s
```

Here, we should check for the older ones first.
But if you're hunting for recoverable space,
you'd rather check for the Burundi VMs first, as they are usually superfat :p

Let's check whether there's really no relevant container left:

```text
root@madrid-prod:/mnt/demostorage# docker ps -a | fgrep 6096471ec44444649d4d11cb03887bb2
7c4f06e7d8f9    a19da31e2b42    "/bin/sh -c 'exec /u…"    4 months ago    Exited (0) 4 months ago    idc-fra-trainingtm.6096471ec44444649d4d11cb03887bb2.olip.builder
```

Check how much space is recoverable:

```text
root@madrid-prod:/mnt/demostorage# du -sh 6096471ec44444649d4d11cb03887bb2
14G 6096471ec44444649d4d11cb03887bb2
```

Just do it!

```text
root@madrid-prod:/mnt/demostorage# rm -rf 6096471ec44444649d4d11cb03887bb2
root@madrid-prod:/mnt/demostorage# df -h /mnt/demostorage/
Filesystem      Size  Used Avail Use% Mounted on
/dev/nvme1n1    4.9T  3.5T  1.2T  76% /mnt/demostorage
```
