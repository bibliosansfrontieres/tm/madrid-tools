#!/bin/bash


DEMOSTORAGE_PATH='/mnt/demostorage'
BACKUP_PATH='/mnt/demostorage/backup-app.db'


process_id=$1
[ "${process_id}" == "" ] && {
    >&2 echo "Error: feed me with a process_id plz"
    exit 1
}

source_db_file="${DEMOSTORAGE_PATH}/${process_id}/app.db"
[ -r "${source_db_file}" ] || {
    >&2 echo  "Error: could not find ${process_id}. Aborting."
    exit 2
}

[ -r "${BACKUP_PATH}" ] || {
    >&2 echo "Error: target directory is not writable: ${BACKUP_PATH}"
    exit 3
}
omekaid_and_name_and_owner=$(
    curl --silent --insecure "https://front.tm.bsf-intranet.org/goto/temps-modernes/api/process/?id=${process_id}" \
        | jq -r '(.project.id | tostring)  + "." + .deployment.cap_config.project_name + "-" + .user.username'
    )
ts="$( date +%Y%m%d%H%M%S )"
target_db_file="${BACKUP_PATH}/${process_id}.${omekaid_and_name_and_owner}.${ts}.db"


cp "${DEMOSTORAGE_PATH}/${process_id}/app.db" "${target_db_file}" || {
    >&2 echo "Error: could not copy to ${target_db_file}"
    exit 4
}
>&2 echo "[+] DB saved to ${target_db_file}"
