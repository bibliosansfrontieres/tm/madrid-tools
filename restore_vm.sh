#!/bin/bash

# config
demostorage_path='/mnt/demostorage'


# args
source_db_file=$1
target_process_id=$2

show_usage() {
    >&2 echo "Usage: $( basename "$0" ) <backup DB file path> <target process_id>

    Example: $( basename "$0" ) /mnt/demostorage/backup-app.db/idb-bdi-education-6085e174063d4d388536448298ee1ba3-20210323-app.db 734878d9f72f4f8e92405babac4da457"
}


# check user input
[ $# -ne 2 ] && {
    >&2 echo "Error: missing args."
    show_usage
    exit 1
}
[ -r "${source_db_file}" ] || {
    >&2 echo "Error: file is not readable: ${source_db_file}. Aborting."
    exit 2
}
[ -d "${demostorage_path}/${target_process_id}" ] || {
    >&2 echo "Error: Can't find this process_id: ${target_process_id}. Aborting."
    exit 3
}
type -t sqlite3 || {
    >&2 echo "Error: sqlite3(1) is not installed. Aborting."
    exit 4
}

# query app.db ".schema table"
query() {
    local db=$1
    local query=$2
    echo "$query" | sqlite3 "$db"
}

echo "source db file: $source_db_file"
echo "target process_id path: ${demostorage_path}/${target_process_id}"
echo

# build more args
target_db_file="${demostorage_path}/${target_process_id}/app.db"



echo "[+] Stopping API and mediacenter.app containers..."
api_ct=$( docker ps --filter "name=api\..*\.${target_process_id}\.olip" --format "{{ .Names }}" )
hugo_ct="mediacenter.app.mediacenter.${target_process_id}"
docker stop "${api_ct}" "${hugo_ct}"

echo "[+] Backup target database..."
cp "${target_db_file}" "${target_db_file}-before-restore"

echo "[+] Copying databse..."
cp "${source_db_file}" "${target_db_file}"



echo "[+] Fixing containers..."

old_process_id=$(
    query "${source_db_file}" "select name from installed_containers ;" \
        | cut -d '.' -f 4 \
        | sort --unique
)
echo "[>] old process_id from ${source_db_file}: ${old_process_id}"
echo

ct_list=$( query "${source_db_file}" "select name from installed_containers ;" )
printf "[>] Containers from %s:\n%s\n" "${source_db_file}" "${ct_list}"

for old_name in ${ct_list} ; do

    >&2 echo  "  [+] Fix name for ${old_name}..."
    # shellcheck disable=SC2001  # keep it readable
    new_name=$(
        echo "${old_name}" \
            | sed -e "s/${old_process_id}/${target_process_id}/"
    )
    query "${target_db_file}" "update installed_containers set name = '${new_name}' where name = '${old_name}' ;"

    >&2 echo "  [+] Fix port for ${old_name}..."
    port=$( query "${target_db_file}-before-restore" "select host_port from installed_containers where name = '${new_name}';" )
    [ -z "$port" ] && >&2 echo "container not found, ignoring..." && continue
    query "${target_db_file}" "update installed_containers set host_port = '${port}' where name = '${new_name}' ;"

done



echo "[+] Fixing contents..."

content_path="${demostorage_path}/${target_process_id}/mediacenter.app/content"

for destination_path in $( query "${target_db_file}" 'select destination_path from installed_contents where current_state = "installed"  ;' ) ; do

    [ -d "${content_path}/${destination_path}" ] && continue
    >&2 echo "Content not found on filesystem: ${destination_path}"
    query "${target_db_file}" "update installed_contents set current_state = 'uninstalled' where  destination_path = '${destination_path}' ;"

done

echo '[+] Force rebuild the mediacenter...'
touch "${content_path}/has_been_added"

echo '[+] Restarting API and mediacenter containers...'
docker start "${api_ct}" "${hugo_ct}"

echo "[+] Done. You should keep an eye on API's logs: docker logs --tail 20 --follow ${api_ct}"
